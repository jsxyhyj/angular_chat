import { Component, OnInit } from '@angular/core'

// 引入状态管理
import { Store } from '@ngrx/store'

@Component({
    selector: 'app-uinfo',
    template: `
        <div class="wcim__topBar"> <div class="inner flexbox flex-alignc">  <a class="linkico wcim__ripple-fff" (click)="routeBack()"><i class="iconfont icon-back"></i></a>  <h2 class="barTit sm flex1">
                    <div class="barCell flexbox flex__direction-column"><em class="clamp1">详细资料</em></div>
                </h2> <a class="linkico wcim__ripple-fff" href="javascript:;"><i class="iconfont icon-dots"></i></a>
            </div>
        </div>
        <div class="wc__ucinfo-detail"> <ul class="clearfix">  <li>
                    <div class="item flexbox flex-alignc wcim__material-cell"> <img class="uimg" src="../../assets/img/uimg/u__chat-img15.jpg" />
                        <label class="lbl flex1"><em class="fs-36">萌帅~</em><em class="iconfont icon-male fs-32 ml-10" style="color:#46b6ef"></em><i>昵称：阳光少年</i></label>
                    </div>
                </li>
                <li> <div class="item flexbox flex-alignc wcim__material-cell">  <label class="lbl flex1">设置备注名和描述</label>
                    </div> </li>
                <li>
                    <div class="item flexbox flex-alignc wcim__material-cell">  <label class="lbl">地区</label>  <div class="cnt flex1 c-999">韩国</div>
                    </div>
                    <div class="item flexbox flex-alignc wcim__material-cell"> <label class="lbl">电话</label>
                        <div class="cnt flex1 c-999">86+ {{auth.user}}</div> </div>
                    <div class="item flexbox flex-alignc wcim__material-cell"> <label class="lbl flex1">更多</label>
                    </div>
                </li>
            </ul>
            <div class="wc__btns-panel"> <a href="javascript:;" class="wc__btn-primary">发消息</a> <a href="javascript:;" class="wc__btn-default">视频聊天</a>
            </div>
        </div>
    `,
    styles: [``]
})
export class UinfoComponent implements OnInit {
    private auth: any
    constructor(  private store: Store<{}>
    ) {
        let that = this
        this.store.select('auth').subscribe(v => { that.auth = v;
        })
    }
    ngOnInit(): void { }
    routeBack(){
        history.go(-1)
    }
}
