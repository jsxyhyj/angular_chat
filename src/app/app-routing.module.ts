import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { Auth } from '../views/auth/auth'

import { NotFoundComponent } from '../components/404'
import { LoginComponent } from '../views/auth/login'
import { RegisterComponent } from '../views/auth/register'
import { IndexComponent } from '../views/index'
import { ContactComponent } from '../views/contact'
import { UinfoComponent } from '../views/contact/uinfo'
import { UcenterComponent } from '../views/ucenter'
import { GroupChatComponent } from '../views/chat/group-chat'
import { GroupInfoComponent } from '../views/chat/group-info'
import { SingleChatComponent } from '../views/chat/single-chat'


export const routes: Routes = [
  {
    path: '', redirectTo: 'index', pathMatch: 'full',data: { showHeader: true, showTabBar: true },
  },
  { path: 'login', component: LoginComponent,
  },
  { path: 'register', component: RegisterComponent,
  },
  {
    path: 'index', component: IndexComponent, canActivate: [Auth],data: { showHeader: true, showTabBar: true },
  },
  {
    path: 'contact', component: ContactComponent, canActivate: [Auth], data: { showHeader: true, showTabBar: true },
  },
  {
    path: 'contact/uinfo', component: UinfoComponent
  },
  {
    path: 'ucenter', component: UcenterComponent, canActivate: [Auth], data: { showHeader: false, showTabBar: true },
  },
  {
    path: 'chat/group-chat', component: GroupChatComponent, canActivate: [Auth]
  },
  {
    path: 'chat/single-chat', component: SingleChatComponent, canActivate: [Auth]
  },
  {
    path: 'chat/group-info', component: GroupInfoComponent, canActivate: [Auth]
  },

  {
    path: '**', component: NotFoundComponent,
  },

  // ...
];

@NgModule({
  // imports: [RouterModule.forRoot(routes)],
  imports: [RouterModule.forRoot(routes, { useHash: true })],  //开启hash模式
  exports: [RouterModule],
  providers: [Auth]
})
export class AppRoutingModule {}
