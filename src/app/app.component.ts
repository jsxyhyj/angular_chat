import { Component } from '@angular/core';
import '../assets/js/fontSize'
import * as $ from 'jquery'
// 引入wcPop插件
import '../assets/js/wcPop/wcPop'
declare var wcPop: any
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  constructor(){}
  ngOnInit(){
    $("body").on("contextmenu", ".weChatIM__panel", function (e) {
      e.preventDefault();
    });

    $("body").on("click", "#J__topbarAdd", function (e) {
        var _points = [$(this).offset().left + $(this).width() - 6, $(this).offset().top + $(this).height()];
        var contextMenuIdx = wcPop({
            skin: 'contextmenu',
            shade: true,
            shadeClose: true,
            follow: _points,
            opacity: 0,

            style: 'background:#378fe7; min-width:3.2rem;',
            btns: [
                {
                    text: '<i class="iconfont icon-choose fs-40 mr-10"></i><span>添加好友</span>',
                    style: 'color:#fff;',
                    onTap() {
                        wcPop.close(contextMenuIdx);
                        // 添加好友
                        var addFriendIdx = wcPop({
                            id: 'wcim_fullscreen',
                            skin: 'fullscreen',
                            title: '添加好友',
                            content: $("#J__popupTmpl-addFriends").html(),
                            position: 'right',
                            opacity: .1,
                            xclose: true,
                            style: 'background: #f5f7fb;',

                            swipe: [
                            {
                                direction: 'right', fn() {wcPop.close()}
                            }
                            ]
                        });
                    }
                },
                {
                    text: '<i class="iconfont icon-qunzu fs-40 mr-10"></i><span>发起群聊</span>',
                    style: 'color:#fff;',
                    onTap() {
                        wcPop.close(contextMenuIdx);
                        // 发起群聊
                        var groupChatIdx = wcPop({
                            id: 'wcim_fullscreen',
                            skin: 'fullscreen',
                            title: '发起群聊',
                            content: $("#J__popupTmpl-launchGroupChat").html(),
                            position: 'right',
                            opacity: .1,
                            xclose: true,
                            style: 'background: #f5f7fb;',

                            swipe: [
                            {
                                direction: 'right', fn() {wcPop.close()}
                            }
                            ]
                        });
                    }
                },
                {
                    text: '<i class="iconfont icon-wenhao fs-40 mr-10"></i><span>帮助</span>',
                    style: 'color:#fff;',
                }
            ]
        });
    });

    $("body").on("click", "#J__groupChatList .group-table:not(.qun) .item", function () {
        $(this).toggleClass("selected");
        var len = $("#J__groupChatList .group-table:not(.qun) .item.selected").size();
        if (len > 0) {
            $(".btn-sure").removeClass("disabled");
            $(".groupChatNum").text('(' + len + ')');
        } else {
            $(".btn-sure").addClass("disabled");
            $(".groupChatNum").text('');
        }
    });
  }
}
